<?php namespace Theme;

use Philo\Blade\Blade;

use Theme\Provider\AssetProvider;

use Theme\Plugin\CustomLogin;
use Theme\Plugin\EditorStyles;
use Theme\Plugin\CustomPageBuilder;
use Theme\Plugin\Gallery\BootstrapGallery;

use Theme\Traits\PostTypes;
use Theme\Traits\Menus;

class App
{
    use PostTypes, Menus;
    /**
     * Initialize the Theme
     *
     * @return void
     */
    public static function boot()
    {
        global $app;
        $app         = new App();
        $app->config = include_once get_template_directory()."/config/app.php" ;

        // Language.
        load_theme_textdomain('core', get_template_directory().'/app/languages');

        return $app->initialize();
    }

    public function initialize()
    {
        $this->checkRequirements();
        $this->setupBlade();

        // Boot providers.
        $this->assets = AssetProvider::boot();

        // Load plugins.
        $this->plugins["custom_login"]      = CustomLogin::load();
        $this->plugins["editor_styles"]     = EditorStyles::load();
        $this->plugins["bootstrap_gallery"] = BootstrapGallery::load();
        $this->plugins["pagebuilder"]       = CustomPageBuilder::load();

        add_filter('upload_mimes', [$this, 'updateMimetypes']);

        return $this;
    }

    public function updateMimetypes($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    /**
     * Checks if the minimum requirements have been set
     * and will try to fix missing dependencies.
     *
     * @return void
     */
    private function checkRequirements()
    {
        $plugins_folder = WP_CONTENT_DIR.'/plugins';
        $this->findOrCreate($plugins_folder);
    }

    /**
     * Initializes Blade templating engine.
     *
     * @return void
     */
    private function setupBlade()
    {
        $views_folder = get_template_directory().'/view';
        $cache_folder = get_template_directory().'/cache';

        global $blade;
        $blade = new Blade(
            $this->findOrCreate($views_folder),
            $this->findOrCreate($cache_folder)
        );
    }

    /**
     * Find or create a file;
     *
     * @param string $path Path to find or create.
     *
     * @return string
     */
    private function findOrCreate($path)
    {
        if (!file_exists($path)) {
            mkdir($path, 0755, true);
        }

        return $path;
    }
}
