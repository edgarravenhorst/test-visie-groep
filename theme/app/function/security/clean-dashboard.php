<?php
function custom_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); //Right Now - Comments, Posts, Pages at a glance
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); //Recent Comments
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); //Incoming Links
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); //Plugins - Popular, New and Recently updated WordPress Plugins
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // Activity
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); //Wordpress Development Blog Feed
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); //Other WordPress News Feed
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); //Quick Press Form
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); //Recent Drafts List
}
add_action('wp_dashboard_setup', 'custom_dashboard_widgets');


function remove_menus(){
  remove_menu_page( 'edit-comments.php' );          //Comments  
}
add_action( 'admin_menu', 'remove_menus' );
