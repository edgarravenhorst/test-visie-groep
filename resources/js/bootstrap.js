window._ = require("lodash");

window.$ = window.jQuery = jQuery || require("jquery");
window.Tether = require("tether");

require("bootstrap");

import solid from "@fortawesome/fontawesome-free-solid";
import fontawesome from "@fortawesome/fontawesome";

fontawesome.library.add(solid);

jQuery(document).ready(function() {});
