<header class="widget widget-header" style="background-image:url({{ $background_image }})">
  <section class="inner">

    <div class="content-1">
      @if(!empty($content1))
        {!! $content1 !!}
       @else 
        <h1>{!! the_title() !!}</h1>
      @endif
    </div>

  @if(!empty($content2))
    <div class="content-2">
      {!! $content2 !!}
    </div>
  @endif
  </section>
</header>

<header class="widget widget-header-secondary">
  <section class="inner">
    <a class="btn btn-lg btn-secondary" href="/shop">Bekijk onze lakens</a>
  </section>
</header>