require("./bootstrap");
import FixedOnScroll from "./components/fixed_on_scroll.js";

const Menu = require("advanced-menus");

jQuery(document).ready(function() {
  const fixedSubMenu = new FixedOnScroll("header#navigation", 150);

  var mainmenu = new Menu({
    options: {
      parent_clickable: true,
      disable_scroll: true
    },
    selectors: {
      toggle_button: false,
      open_button: ".toggle-mainmenu-button .open-button",
      close_button: ".toggle-mainmenu-button .close-button"
    },
    events: {
      swipeLeft: function(menu, e) {
        menu.close();
      },
      swipeRight: function(menu, e) {
        if (e.touches[0].clientX < 50) {
          menu.open();
        }
      }
    }
  });
});
