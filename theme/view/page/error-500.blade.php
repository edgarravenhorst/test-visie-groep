@extends('layout.default')

@section("page-content")
  <div class="alert alert-warning">
    {{ __('Sorry, er lijkt iets mis te gaan met de pagina die je probeert op te halen.', 'wpcore2019') }}
  </div>
  {!! get_search_form(false) !!}
@endsection
