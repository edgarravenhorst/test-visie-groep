<?php

return [
    "style" => [
        "colors"         => include_once "colors.php",
        "editor_formats" => include_once "editor_formats.php",
    ],
];
