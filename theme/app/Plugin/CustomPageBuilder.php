<?php namespace Theme\Plugin;

class CustomPageBuilder
{
    /**
     * Load this plugin
     *
     * @return void
     */
    public static function load()
    {
        $plugin = new CustomPageBuilder();
        return $plugin->initialize();
    }

    public function initialize()
    {
        // Add filters for our widgets
        add_filter('coderehab_widget_content', 'wptexturize');
        add_filter('coderehab_widget_content', 'convert_smilies');
        add_filter('coderehab_widget_content', 'convert_chars');
        add_filter('coderehab_widget_content', 'wpautop');
        add_filter('coderehab_widget_content', 'shortcode_unautop');
        add_filter('coderehab_widget_content', 'do_shortcode');

        //Pagebuilder configuration
        add_filter('siteorigin_widgets_widget_folders', [$this, 'register_widget_folder']);
        add_filter('siteorigin_widgets_active_widgets', [$this, 'activate_widgets']);

        add_filter('siteorigin_panels_layout_data', [$this, 'update_layout_data'], 10, 99);
        add_filter('siteorigin_panels_css_object', [$this, 'update_styles'], 10, 99);

        add_filter('siteorigin_panels_row_classes', [$this, 'update_row_classes'], 10, 99);
        add_filter('siteorigin_panels_row_attributes', [$this, 'update_row_attributes'], 10, 99);
        add_filter('siteorigin_panels_row_style_css', [$this, 'update_row_css'], 10, 99);
        //
        add_filter('siteorigin_panels_cell_classes', [$this, 'update_cell_classes'], 10, 99);
        add_filter('siteorigin_panels_cell_attributes', [$this, 'update_cell_attributes'], 10, 99);
        add_filter('siteorigin_panels_cell_style_css', [$this, 'update_cell_css'], 10, 99);

        add_filter('siteorigin_panels_before_content', [$this, 'before_layout'], 10, 10);
        add_filter('siteorigin_panels_after_content', [$this, 'after_layout'], 10, 10);

        add_filter('siteorigin_panels_before_row', [$this, 'before_row'], 10, 10);
        add_filter('siteorigin_panels_after_row', [$this, 'after_row'], 10, 10);

        add_filter('siteorigin_panels_before_cell', [$this, 'before_cell'], 10, 10);
        add_filter('siteorigin_panels_after_cell', [$this, 'after_cell'], 10, 10);

        add_filter('siteorigin_panels_row_style_attributes', [$this, 'update_row_aside_attributes']);
        add_filter('siteorigin_panels_row_style_fields', [$this, 'update_row_aside_form']);

        add_filter('siteorigin_panels_cell_style_attributes', [$this, 'update_cell_aside_attributes'], 10, 2);
        add_filter('siteorigin_panels_cell_style_fields', [$this, 'update_cell_aside_form'], 10, 3);

        add_filter('siteorigin_panels_general_style_css', [$this, 'remove_style_wrapper'], 10, 3);
        add_filter('siteorigin_panels_general_style_mobile_css', [$this, 'remove_style_wrapper'], 10, 3);
        add_filter('siteorigin_panels_general_style_attributes', [$this, 'remove_style_wrapper'], 10, 3);
    }

    public function remove_style_wrapper($attributes, $style)
    {
        return null;
    }

    public function update_styles($css, $panels_data, $post_id, $layout_data)
    {
        $css      = new \SiteOrigin_Panels_Css_Builder();
        $settings = siteorigin_panels_setting();

        foreach ($layout_data as $row_key => $row) {
            $standard_css = apply_filters('siteorigin_panels_row_style_css', [], $row['style']);
            $mobile_css   = apply_filters('siteorigin_panels_row_style_mobile_css', [], $row['style']);
            $row_id       = $row['style']['id'] ? $row['style']['id'] : $row['id'];
            if (!empty($row['style']['gutter'])) {
                $gutter = $result = preg_split('/(-?[0-9]+\.?[0-9]*)/i', $row['style']['gutter'], 0, PREG_SPLIT_DELIM_CAPTURE);
            }

            $container_css = [];
            if (!empty($standard_css['padding'])) {
                $container_css['padding'] = $standard_css['padding'];
            }

            $container_mobile_css = [];
            if (!empty($mobile_css['padding'])) {
                $container_mobile_css['padding'] = $mobile_css['padding'];
            }

            if (!empty($container_css)) {
                $css->add_css("#" . $row_id . " > .container-fluid", $container_css, 9999);
            }

            if (!empty($container_mobile_css)) {
                $css->add_css("#" . $row_id . " > .container-fluid", $container_mobile_css, 720);
            }

            $row_css = [];
            if (!empty($row['style']['cell_alignment'])) {
                $row_css['align-items'] = $row['style']['cell_alignment'];
            }

            if (!empty($row['style']['gutter'])) {
                $row_css['margin-left']  = (-$gutter[1] / 2) . $gutter[2];
                $row_css['margin-right'] = (-$gutter[1] / 2) . $gutter[2];
            }

            if (!empty($row_css)) {
                $css->add_css("#" . $row_id . " > .container-fluid > .row", $row_css, 9999);
            }

            $col_css = [];
            if (!empty($row['style']['gutter'])) {
                $col_css['padding-left']  = ($gutter[1] / 2) . $gutter[2];
                $col_css['padding-right'] = ($gutter[1] / 2) . $gutter[2];
            }

            if (!empty($col_css)) {
                $css->add_css("#" . $row_id . " > .container-fluid > .row > .col", $col_css, 9999);
            }

            unset($standard_css['padding']);
            unset($mobile_css['mobile_padding']);

            if (!empty($row['style']['bottom_margin'])) {
                $standard_css['margin-bottom'] = $row['style']['bottom_margin'];
            }

            if (!empty($standard_css)) {
                $css->add_css("#" . $row_id, $standard_css, 9999);
            }

            if (!empty($mobile_css)) {
                $css->add_css("#" . $row_id, $mobile_css, $settings['mobile-width']);
            }

            foreach ($row['cells'] as $cell_key => $cell) {
                $cell_mobile_css = apply_filters('siteorigin_panels_cell_style_mobile_css', [], $cell['style']);
                $cell_css = apply_filters('siteorigin_panels_cell_style_css', [], $cell['style']);
                $cell_id  = $post_id . '-' . $row_key . '-' . $cell_key;

                if (!empty($cell['style']['vertical_alignment'])) {
                    $cell_css['justify-content'] = $cell['style']['vertical_alignment'];
                }

                if (!empty($cell_css)) {
                    $css->add_css('#pgc-' . $cell_id, $cell_css, 9999);
                }

                if (!empty($cell_mobile_css)) {
                    $css->add_css('#pgc-' . $cell_id, $cell_mobile_css, 720);
                }

                foreach ($cell['widgets'] as $widget_key => $widget) {
                    $widget_mobile_css = apply_filters('siteorigin_panels_widget_style_mobile_css', [], $widget['panels_info']['style']);
                    $widget_css = apply_filters('siteorigin_panels_widget_style_css', [], $widget['panels_info']['style']);
                    $widget_id  = $post_id . '-' . $row_key . '-' . $cell_key . '-' . $widget_key;

                    if (!empty($widget['panels_info']['style']['margin'])) {
                        $widget_css['margin'] = $widget['panels_info']['style']['margin'];
                    }

                    if (!empty($widget_css)) {
                        $css->add_css('#panel-' . $widget_id, $widget_css, 9999);
                    }

                    if (!empty($widget['panels_info']['style']['link_color'])) {
                        $css->add_css('#panel-' . $widget_id . ' a', array(
                            'color' => $widget['panels_info']['style']['link_color'],
                        ), 9999);
                    }

                    if (!empty($widget_mobile_css)) {
                        $css->add_css('#panel-' . $widget_id, $widget_mobile_css, 720);
                    }
                }
            }
        }

        return $css;
    }

    public function activate_widgets($widgets)
    {
        $widgets['coreflick-slider'] = true;
        return $widgets;
    }

    public function register_widget_folder($folders)
    {
        $folders[] = get_template_directory().'/widgets/custom/';
        return $folders;
    }

    public function update_layout_data($layout_data, $post_id)
    {
        foreach ($layout_data as $key => $row) {
            $layout_data[$key]["id"] = $row["id"] ? $row["id"] : "section-$post_id-" . ($key + 1);
        }
        return $layout_data;
    }

    public function update_row_classes($class, $row_data)
    {
        $class[] = "row";
        return $class;
    }

    public function update_row_attributes($attributes, $row_data)
    {
        return $attributes;
    }

    public function update_row_css($css, $style)
    {
        return $css;
    }

    public function update_row_aside_attributes($fields)
    {
        return $fields;
    }

    public function update_row_aside_form($fields)
    {
        unset($fields["row_stretch"]);

        $fields['row-size'] = [
            "name"     => "Container",
            "type"     => "select",
            "group"    => "layout",
            "options"  => [
                "default" => "Default",
                "none"    => "None",
                "stretch" => "Stretch",
                "wide"    => "Wide",
                "narrow"  => "Narrow",
            ],

            "priority" => 1,
        ];

        return $fields;
    }

    public function update_cell_aside_attributes($fields)
    {
        return $fields;
    }

    public function update_cell_aside_form($fields)
    {
        return $fields;
    }

    public function update_cell_classes($class, $cell)
    {
        $class[] = "inner";
        return $class;
    }

    public function update_cell_attributes($attributes, $cell)
    {
        return $attributes;
    }

    public function update_cell_css($css, $style)
    {
        return $css;
    }

    //----------------------------------------------
    // Additional wrappers
    //----------------------------------------------

    public function before_layout($attributes, $grid, $c)
    {
        return "";
    }

    public function after_layout($attributes, $grid)
    {
        return "";
    }

    public function before_row($before, $row, $c)
    {
        $id     = $row['style']['id'] ? "id='" . $row['style']['id'] . "'" : "id='" . $row['id'] . "'";
        $before = $before . "<section $id>";

        if (!$row["style"] || !$row["style"]["row-size"] || $row["style"]["row-size"] !== "none") {
            $size = $row["style"]["row-size"] ? $row["style"]["row-size"] : "";
            return $before . "<div class='container-fluid $size' style=''>";
        }

        return $before;
    }

    public function after_row($attributes, $row)
    {
        if (!$row["style"] || !$row["style"]["row-size"] || $row["style"]["row-size"] !== "none") {
            return "</div> </section>";
        }

        return "</section>";
    }

    public function before_cell($attributes, $grid, $c)
    {
        $desktop_cols = intval(round($grid["weight"] * 12));
        $tablet_cols  = $desktop_cols * 2 < 12 ? $desktop_cols * 2 : 12;
        $mobile_cols  = $desktop_cols * 3 < 12 ? $desktop_cols * 3 : 12;

        if ($mobile_cols > 6) {
            $mobile_cols = 12;
        }

        if ($tablet_cols > 6) {
            $tablet_cols = 12;
        }

        $class = "col col-$mobile_cols col-md-$tablet_cols col-lg-$desktop_cols";

        return "<div class='$class' >";
    }

    public function after_cell($attributes, $grid)
    {
        return "</div>";
    }
}
