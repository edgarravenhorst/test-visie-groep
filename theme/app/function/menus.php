<?php
//register menus
function register_default_menus() {
    register_nav_menu( 'main-menu', 'Hoofdnavigatie' );
}
add_action( 'after_setup_theme', 'register_default_menus' );
