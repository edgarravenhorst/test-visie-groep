<?php namespace Theme\Plugin\Gallery;

class BootstrapGallery
{
    /**
     * Load this plugin
     *
     * @return void
     */
    public static function load()
    {
        $plugin = new BootstrapGallery();
        $plugin->initialize();
        return $plugin;
    }

    /**
     * Initialize this plugin
     *
     * @return void
     */
    public function initialize()
    {
        add_action('wp_enqueue_scripts', [$this, "registerScripts"]);
        add_action('wp_enqueue_scripts', [$this, "registerStyles"]);
        add_filter('post_gallery', 'bootstrap_gallery', 10, 4);
    }

    /**
     * Register scripts for this plugin
     *
     * @return void
     */
    public function registerScripts()
    {
        global $app;

        $fancybox_path = $app->assets->theme_dir."/app/Plugin/Gallery/assets/js/jquery.fancybox.min.js";
        $app->assets->registerStyle("css-core-fancybox", $fancybox_path);
    }

    /**
     * Register styles for this plugin
     *
     * @return void
     */
    public function registerStyles()
    {
        global $app;

        $fancybox_path = $app->assets->theme_dir."/app/Plugin/Gallery/assets/css/jquery.fancybox.min.css";
        $app->assets->registerStyle("js-core-fancybox", $fancybox_path);
    }

    /**
     * Function that's called on post_gallery.
     *
     * @param string $output   The rendered Html
     * @param array  $atts     TBD
     * @param array  $instance TBD
     *
     * @return string
     */
    public function applyFilter($output, $atts, $instance)
    {
        $return = '
        <script type="text/javascript">
            $("[data-fancybox]").fancybox({
                // Options will go here
            // http://fancyapps.com/fancybox/3/docs/
            });
        </script>
        ';

        $imagesize = $atts['size'];
        $order     = $atts['orderby'];

        if (strlen($atts['columns']) < 1) {
            $columns = 3;
        } else {
            $columns = $atts['columns'];
        }

        $images = explode(',', $atts['ids']);
        if ($columns < 1 || $columns > 12) {
            $columns == 3;
        }

        // Only use user input columns for large screens.
        $col_class = 'col-sm-12 col-md-6 col-lg-'.(12 / $columns);
        $return   .= '<div class="row gallery">';
        $i         = 0;

        // If random option is checked.
        if ($order == "rand") {
            shuffle($images);
        }

        foreach ($images as $key => $value) {
            if (($i % $columns) == 0 && $i > 0) {
                $return .= '</div><div class="row gallery">';
            }

            $image_attributes = wp_get_attachment_image_src($value, $imagesize);
            $full_image       = wp_get_attachment_image_src($value, 'full-size');

            $return .= '
            <div class="'.$col_class.'">
            <div class="gallery-image-wrap mb-4">
                <a class="fancybox" data-fancybox="gallery-'.$atts['ids'].'" href="'.$full_image[0].'">
                <img src="'.$image_attributes[0].'" alt="" class="img-responsive">
                </a>
            </div>
            </div>';

            $i++;
        }

        $return .= '</div>';
        return $return;
    }
}
