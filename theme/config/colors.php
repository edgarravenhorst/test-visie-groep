<?php

return [
    [
        "name"  => "Black",
        "color" => "000000",
    ],
    [
        "name"  => "White",
        "color" => "ffffff",
    ],
    [
        "name"  => "Light Gray",
        "color" => "F2F2F2",
    ],
    [
        "name"  => "Dark Gray",
        "color" => "3f3c3a",
    ],
    [
        "name"  => "Code.Rehab Blue",
        "color" => "22365D",
    ],
    [
        "name"  => "Code.Rehab light Blue",
        "color" => "FF0000",
    ],
];
