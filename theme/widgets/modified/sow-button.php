<?php
class SowButtonModification
{
    public function __construct()
    {
        add_filter('siteorigin_widgets_template_html_sow-button', array($this, "render_blade_template"), 10, 3);
        add_filter('siteorigin_widgets_less_sow-button', array($this, "reset_styles"), 10, 3);
        add_filter('siteorigin_widgets_form_options_sow-button', array($this, "update_form"), 10, 3);
    }

    public function render_blade_template($template_html, $instance, $widget)
    {
        global $blade;
        return $blade->view()->make("widget.widget-sow-button", $instance);
    }

    public function reset_styles($less, $instance, $widget)
    {
        // dd($less, $instance, $widget);

        return "";
    }

    public function update_form($form, $widget)
    {

        $form["design"]["fields"]["theme"] = [
            "type"    => "select",
            "label"   => "Button theme",
            "default" => "btn-primary",
            "options" => [
                "btn-primary" => "Primary",
                "btn-outline-primary" => "Primary Outline",
                "btn-secondary" => "Secondary",
                "btn-outline-secondary" => "Secondary Outline",
                "btn-tertiary" => "Tertiary",
                "btn-tertiary-secondary" => "Tertiary Outline",
                "btn-white" => "White",
            ],
        ];

        $form["design"]["fields"]["size"] = [
            "type"    => "select",
            "label"   => "Button size",
            "default" => "",
            "options" => [
                "" => "Default",
                "btn-sm" => "Small",
                "btn-lg" => "Large",
                "btn-block" => "Full width",
            ],
        ];

        unset($form["design"]['fields']['width']);
        unset($form["design"]['fields']['button_color']);
        unset($form["design"]['fields']['text_color']);
        unset($form["design"]['fields']['hover']);
        unset($form["design"]['fields']['font']);
        unset($form["design"]['fields']['font_size']);
        unset($form["design"]['fields']['rounding']);
        unset($form["design"]['fields']['padding']);
        return $form;
    }
}

$custom_sow_button = new SowButtonModification();
