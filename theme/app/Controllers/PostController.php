<?php
namespace Theme\Controllers;

class PostController
{

    /**
     * Render View for the single template
     *
     * @param string $view  Blade template path
     * @param array  $props Params passed to the Blade template
     *
     * @return HTML
     */
    protected function render($view, $props)
    {
        global $blade;
        return $blade->view()->make($view, $props)->render();
    }

    /**
     * Render View for the single template
     *
     * @return HTML
     */
    public function single()
    {
        $props = [];
        return $this->render('page.single', $props);
    }

    /**
     * Render View for the page template
     *
     * @return HTML
     */
    public function page()
    {
        $props = [];
        return $this->render('page.page', $props);
    }

    /**
     * Render View for the archive template
     *
     * @return HTML
     */
    public function archive()
    {
        $props = [];
        return $this->render('page.archive', $props);
    }

    /**
     * Render View for the tag template
     *
     * @return HTML
     */
    public function tag()
    {
        $props = [];
        return $this->render('page.archive', $props);
    }

    /**
     * Render View for the category template
     *
     * @return HTML
     */
    public function category()
    {
        $props = [];
        return $this->render('page.archive', $props);
    }

    /**
     * Render View for the 404 template
     *
     * @return HTML
     */
    public function error404()
    {
        $props = [];
        return $this->render('page.error-404', $props);
    }

    /**
     * Render View for the 500 template
     *
     * @return HTML
     */
    public function error500()
    {
        $props = [];
        return $this->render('page.error-500', $props);
    }

    /**
     * Render View for the search template
     *
     * @return HTML
     */
    public function search()
    {
        $props = [];
        return $this->render('page.search', $props);
    }

    /**
     * Render View for the dynamic frontpage template
     *
     * @return HTML
     */
    public function frontpage()
    {
        $props = [];
        return $this->render('page.page', $props);
    }


}
