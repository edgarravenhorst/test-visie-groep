<?php namespace Theme\Plugin;

class CustomLogin
{
    /**
     * Load this plugin
     *
     * @return void
     */
    public static function load()
    {
        $plugin = new CustomLogin();
        $plugin->initialize();
        return $plugin;
    }

    /**
     * Initialize this plugin
     *
     * @return void
     */
    public function initialize()
    {
        add_action('login_enqueue_scripts', [$this, "registerStyles"]);
    }

    /**
     * Register styles for this plugin
     *
     * @return void
     */
    public function registerStyles()
    {
        global $app;

        $style_path = $app->assets->styles_dir."/adminlogin.css";
        $app->assets->registerStyle("css-core-fancybox", $style_path);
    }
}
