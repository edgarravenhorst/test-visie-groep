<?php

return [
    [
        'title' => 'Buttons',
        'items' => [
            [
                'title'    => 'Button',
                'selector' => 'a, button',
                'classes'  => 'btn',
            ],
            [
                'title'    => 'Primary button',
                'selector' => '.btn',
                'classes'  => 'btn-primary',
            ],
            [
                'title'    => 'Secondary button',
                'selector' => '.btn',
                'classes'  => 'btn-secondary',
            ],
            [
                'title'    => 'Primary outline button',
                'selector' => '.btn',
                'classes'  => 'btn-outline-primary',
            ],
            [
                'title'    => 'Primary secondary button',
                'selector' => '.btn',
                'classes'  => 'btn-outline-secondary',
            ],
            [
                'title'    => 'Large button',
                'selector' => '.btn',
                'classes'  => 'btn-lg',
            ],
            [
                'title'    => 'Small button',
                'selector' => '.btn',
                'classes'  => 'btn-sm',
            ],
            [
                'title'    => 'Full width',
                'selector' => '.btn',
                'classes'  => '.btn-block',
            ],
        ],
    ],
    [
        'title' => 'Heading styles',
        'items' => [
            [
                'title'    => 'Uppercase',
                'selector' => '*',
                'classes'  => 'uppercase',
            ],
            [
                'title'    => 'Heading 1 size',
                'selector' => 'h1,h2,h3,h4,h5,h6,p',
                'classes'  => 'h1',
            ],
            [
                'title'    => 'Heading 2 size',
                'selector' => 'h1,h2,h3,h4,h5,h6,p',
                'classes'  => 'h2',
            ],
            [
                'title'    => 'Heading 3 size',
                'selector' => 'h1,h2,h3,h4,h5,h6,p',
                'classes'  => 'h3',
            ],
            [
                'title'    => 'Heading 4 size',
                'selector' => 'h1,h2,h3,h4,h5,h6,p',
                'classes'  => 'h4',
            ],
            [
                'title'    => 'Heading 5 size',
                'selector' => 'h1,h2,h3,h4,h5,h6,p',
                'classes'  => 'h5',
            ],
            [
                'title'    => 'Heading 6 size',
                'selector' => 'h1,h2,h3,h4,h5,h6,p',
                'classes'  => 'h6',
            ],
        ],
    ],
];
