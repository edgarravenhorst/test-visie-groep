@extends('layout.default')

@section("page-content")
  <div class="alert alert-warning">
    {{ __('Sorry, de pagina die je probeert te bereiken kan niet worden gevonden.', 'wpcore2019') }}
  </div>
  {!! get_search_form(false) !!}
@endsection
