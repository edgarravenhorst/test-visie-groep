<?php

/*
Widget Name: Core Header
Description: Summary off Header
Author: Code.Rehab
Author URI: http://code.rehab
 */

class Coderehab_Header extends SiteOrigin_Widget
{
    public function __construct()
    {
        parent::__construct(
            'coderehab-header',
            __('Core Header', 'coderehab-widgets-bundle'),
            array(
                'description' => __('A fullscreen block with big text.', 'coderehab-widgets-bundle'),
                'help'        => __('https://code.rehab'),
            ),
            array(),
            false,
            plugin_dir_path(__FILE__)
        );
    }

    public function initialize()
    {
        add_image_size('banner-image', 1920, 1080, array('center', 'center')); // maybe better to select one from all registered sizes
        add_filter('siteorigin_widgets_template_html_coderehab-header', [$this, "render_blade_template"], 10, 3);

        global $blade;
        $blade->view()->addLocation(__DIR__ . "/view");

        // $this->register_scripts_and_styles();
    }

    public function get_less_variables($instance)
    {
        return [];
    }

    public function render_blade_template($template_html, $instance, $widget)
    {
        $background_image = wp_get_attachment_image_src($instance["background-image"], 'xxl')[0];

        global $blade;
        return $blade->view()->make(
            "widget.core-header",
            [
                "content1"=> $instance["content1"],
                "content2"=> $instance["content2"],
                "background_image"=> $background_image,
            ]
        );
    }

    public function get_widget_form()
    {
        return [
            // Content
            'content1'          => array(
                'type'  => 'tinymce',
                'label' => __('Content1', 'dation-widgets-bundle'),
            ),
            
            'content2'          => array(
                'type'  => 'tinymce',
                'label' => __('Content2', 'dation-widgets-bundle'),
            ),
            'background-image' => array(
                'type'     => 'media',
                'label'    => __('Background image', 'dation-widgets-bundle'),
                'choose'   => __('Select image', 'dation-widgets-bundle'),
                'update'   => __('Update image', 'dation-widgets-bundle'),
                'library'  => 'image',
                'fallback' => true,
            ),
        ];
    }
}

siteorigin_widget_register('coderehab-header', __FILE__, 'Coderehab_Header');
