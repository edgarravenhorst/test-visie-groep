<!doctype html>

<html>
@include("layout.partial.head")

  <body {{body_class() }}>

  @include("layout.partial.header")

  @if(content_is_pagebuilder())
    <div class="page-builder-content">
      @yield("page-content")
    </div>
  @else
    <div class="container-fluid">
      @yield("page-title")
      @yield("page-content")
    </div>
  @endif

  @include('layout.partial.footer')

  {{wp_footer() }}
  </body>
</html>
