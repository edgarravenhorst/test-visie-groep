const mix = require("laravel-mix");
const siteurl = "wpcore.test";

mix.options({
  publicPath: "./", //windows can't mix without this!
  extractVueStyles: false,
  processCssUrls: false,
  purifyCss: false,
  postCss: [require("autoprefixer")],
  clearConsole: true,
  uglify: false
});

//compile js
mix
  .js("resources/js/site.js", "theme/assets/js")
  .extract(["jquery", "tether", "lodash", "bootstrap", "axios"])
  .autoload({
    jquery: ["$", "jQuery", "window.jQuery"],
    tether: ["Tether", "window.Tether"]
  });

//compile styles
mix.sass("resources/sass/site.scss", "theme/assets/css");
mix.sass("resources/sass/wp-editor-style.scss", "theme/assets/css");
mix.sass("resources/sass/adminlogin.scss", "theme/assets/css");

mix.sourceMaps();

mix.copy("./generate/wp-config.php", "./build/wp-config.php");
mix.copy("./generate/.htaccess", "./build/.htaccess");

// Live reloading
mix.browserSync({
  proxy: siteurl,
  injectCss: true,
  files: ["!theme/vendor/**/*", "theme/**/*"]
});
