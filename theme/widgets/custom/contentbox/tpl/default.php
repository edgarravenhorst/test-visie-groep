<article class="widget dation-widget dation-contentbox <?php echo $rounded_corners ? "rounded-corners" : ""?> <?php echo $shadow ? "shadow" : ""?>" <?php echo $style ? $style : ""?>>
  <div class="container-fluid">
    <section class="content">
      <?php if ($content): ?>
        <?php echo apply_filters('dation_widget_content', $content);?>
      <?php endif?>
    </section>

    <?php if ($button): ?>
      <footer>
        <a class="btn btn-lg <?php echo $button_style ? $button_style : "btn-primary"?>" href="<?php sow_esc_url( $link ) ?>" class="btn btn-primary btn-outline"><?php echo $button_text?></a>
      </footer>
    <?php endif?>
  </div>
</article>
