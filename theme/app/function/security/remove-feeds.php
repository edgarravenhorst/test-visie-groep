<?php
// remove various feeds
function wp_disable_feed() {
  wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}

add_action('do_feed', 'wp_disable_feed', 1);
add_action('do_feed_rdf', 'wp_disable_feed', 1);
add_action('do_feed_rss', 'wp_disable_feed', 1);
add_action('do_feed_atom', 'wp_disable_feed', 1);
add_action('do_feed_rss2_comments', 'wp_disable_feed', 1);
add_action('do_feed_atom_comments', 'wp_disable_feed', 1);
//add_action('do_feed_rss2', 'fb_disable_feed', 1);
