<?php
namespace Theme;

require 'vendor/autoload.php';

\Theme\App::boot();

/*
 * global $app;

 * $app->registerMenu($menuprops);
 * $app->registerMenu($menuprops);

 * $app->registerPostType($menuprops);
 * $app->registerPostType($menuprops);
 * $app->registerPostType($menuprops);
 */

// Load helper functions.
require_once "app/Helpers/pagebuilder.php";

// Load theme stuff the old fashioned way.
require_once "app/function/menus.php";
require_once "app/function/basic-security.php";
require_once "app/function/error-pages.php";

// Remove WP Emoji.
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

// Add default posts and comments RSS feed links to head.
add_theme_support('automatic-feed-links');

// Let WordPress manage the document title meta tag.
add_theme_support('title-tag');

// Enable support for Post Thumbnails on posts and pages.
add_theme_support('post-thumbnails');

// Switch markup for search form, comment form, and comments to HTML5.
add_theme_support(
    'html5',
    [
        'search-form',
        'comment-form',
        'comment-list',
        'caption',
    ]
);

// Overwrite Page builder widgets.
require_once "widgets/modified/sow-button.php";
