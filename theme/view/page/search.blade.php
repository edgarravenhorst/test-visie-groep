@extends('layout.default')

@section('page-content')

  <h2>{{ __('Zoekresultaten voor:', 'wpcore2019') }} "{!! esc_html( get_search_query( false ) ) !!}"</h2>
  <hr />


  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, er zijn geen zoekresultaten gevonden', 'wpcore2019') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-loop')
  @endwhile

  {!! get_the_posts_navigation() !!}

@endsection
