<?php namespace Theme\Plugin;

class EditorStyles
{
    /**
     * Load this plugin
     *
     * @return void
     */
    public static function load()
    {
        $plugin = new EditorStyles();
        return $plugin->initialize();
    }

    /**
     * Initialize this plugin
     *
     * @return void
     */
    public function initialize()
    {
        add_action('admin_init', [$this, 'set_custom_editor_stylesheet']);
        add_action('admin_print_footer_scripts', [$this, 'siteorigin_universal_color_pallet']);
        add_action('customize_controls_print_footer_scripts', [$this, 'siteorigin_universal_color_pallet']);

        add_filter('mce_buttons_2', [$this, 'add_editor_styleselect_btn']);
        add_filter('tiny_mce_before_init', [$this, 'setup_custom_editor_settings']);

        return $this;
    }

    public function set_custom_editor_stylesheet()
    {
        add_editor_style('assets/css/wp-editor-style.css');
    }

    public function add_editor_styleselect_btn($buttons)
    {
        array_unshift($buttons, 'styleselect');
        return $buttons;
    }

    public function setup_custom_editor_settings($init_array)
    {
        global $app;
        $style_formats = $app->config['style']['editor_formats'];
        $colors        = $app->config['style']['colors'];

        // dump($app->config);

        $default_colours = '[';

        foreach ($colors as $col) {
            if ($default_colours != "[") {
                $default_colours .= ",";
            }

            $default_colours .= '"'.$col['color'].'", "'.$col['name'].'"';
        }

        $default_colours .= ']';

        // dump($default_colours);

        $init_array['textcolor_map']  = $default_colours;
        $init_array['textcolor_rows'] = 1;
        $init_array['style_formats']  = json_encode($style_formats);

        return $init_array;
    }

    public function siteorigin_universal_color_pallet()
    {
        global $app;
        $colors = $app->config['style']['colors'];


        $render = ' <script>
        jQuery(document).ready(function($){
            $.wp.wpColorPicker.prototype.options = {
                palettes: [';

        foreach ($colors as $col) {
            $render .= "'#".$col['color']."', ";
        }

        $render .= ']}; }); </script>';
        echo $render;
    }
}
