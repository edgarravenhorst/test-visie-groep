<?php
/**
 * Helper function for defining if page content is page builder content.
 *
 * @return boolean
 */
function content_is_pagebuilder()
{
    global $post;

    // The search page reads the first post, might be build with a pagebuilder, so return false
    if (is_search()) {
      return false;
    }

    // Consider empty content the WP editor.
    if (empty($post)) {
        return false;
    }

    // Does pagebuilder content exist in custom fields?
    $panels_data = get_post_meta($post->ID, 'panels_data', true);

    return (empty($panels_data)) ? false : true;
}
