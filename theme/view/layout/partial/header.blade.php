<header id="navigation">
  <div class="row">
    <div class="col-sm-4 col-9 d-flex align-items-end">
      <a id="logo" href="{{ bloginfo('siteurl') }}">
        {{ get_bloginfo('title') }}
      </a>
    </div>
    <div class="col-sm-8 col-3 d-flex align-items-center justify-content-end">
      @include('layout.partial.navigation')
    </div>
  </div>
</header>
