<?php namespace Theme;

class Router
{
    public function url($path, $functionstring)
    {
        $match = $_SERVER['REQUEST_URI'] === $path;
        $this->when($match, $functionstring);
    }

    public function when($condition, $functionstring)
    {
        $functionstring = explode("@", $functionstring);
        $controller = "\Theme\Controllers\\" . $functionstring[0];
        $method = $functionstring[1];

        if ($condition) {
            $controller = new $controller();
            echo $controller->$method();
            exit;
        }
    }
}
