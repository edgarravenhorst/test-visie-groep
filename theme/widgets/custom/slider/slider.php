<?php

/*
Widget Name: Core Slider
Description: A flickity slider
Author: Code.Rehab
Author URI: http://code.rehab
 */

class Coderehab_Slider extends SiteOrigin_Widget
{
    public function __construct()
    {
        parent::__construct(
            'coderehab-slider',
            __('Core Slider', 'coderehab-widgets-bundle'),
            array(
                'description' => __('A fullscreen block with big text.', 'coderehab-widgets-bundle'),
                'help'        => __('https://code.rehab'),
            ),
            array(),
            false,
            plugin_dir_path(__FILE__)
        );
    }

    public function initialize()
    {
        add_image_size('banner-image', 1920, 1080, array('center', 'center')); // maybe better to select one from all registered sizes
        add_filter('siteorigin_widgets_template_html_coderehab-slider', [$this, "render_blade_template"], 10, 3);

        global $blade;
        $blade->view()->addLocation(__DIR__ . "/view");

        $this->register_scripts_and_styles();
    }

    public function get_less_variables($instance)
    {
        return array(
            'showFilter'      => $instance['sliderSettings']['show_filter'],
            'prevNextButtons' => $instance['sliderSettings']['prevNextButtons'],
            'pageDots'        => $instance['sliderSettings']['pageDots'],
            'autoPlay'        => $instance['sliderSettings']['autoPlay'],
            'filter_color'    => $instance['sliderSettings']['filterSettings']['filter_color'],
            'filter_opacity'  => $instance['sliderSettings']['filterSettings']['filter_opacity'],
        );
    }

    public function render_blade_template($template_html, $instance, $widget)
    {
        global $blade;
        return $blade->view()->make("widget.core-slider", array(
            "slides"      => $instance["slides"],
            "showArrows"  => $instance['sliderSettings']["prevNextButtons"],
            "showBullets" => $instance['sliderSettings']["pageDots"],
            "autoPlay"    => $instance['sliderSettings']["autoPlay"],
            "showFilter"  => $instance['sliderSettings']["show_filter"],
        ));
    }

    public function get_widget_form()
    {
        return array(
            'slides'         => array(
                'type'       => 'repeater',
                'label'      => __('Slides.', 'coderehab-widgets-bundle'),
                'item_name'  => __('Slide', 'coderehab-widgets-bundle'),
                'item_label' => array(
                    'selector'     => "[id*='title']",
                    'update_event' => 'change',
                    'value_method' => 'val',
                ),

                'fields'     => array(
                    // Title field
                    'title' => array(
                        'type'  => 'text',
                        'label' => __('Slide title', 'coderehab-widgets-bundle'),
                    ),

                    // Slide image
                    'image' => array(
                        'type'    => 'media',
                        'library' => 'image',
                        'label'   => __('Slide image', 'coderehab-widgets-bundle'),
                    ),
                ),
            ),

            'sliderSettings' => array(
                'type'   => 'section',
                'label'  => __('Slider settings', 'coderehab-widgets-bundle'),
                'hide'   => true,
                'fields' => array(

                    // Autoplay
                    'autoPlay'        => array(
                        'type'    => 'slider',
                        'label'   => __('Autoplay (ms)', 'coderehab-widgets-bundle'),
                        'default' => 6000,
                        'min'     => 0,
                        'max'     => 10000,
                        'integer' => true,
                    ),

                    // Show navigation arrows
                    'prevNextButtons' => array(
                        'type'    => 'checkbox',
                        'label'   => __('Show navigation arrows?', 'coderehab-widgets-bundle'),
                        'default' => true,
                    ),

                    // Show navigation bullets
                    'pageDots'        => array(
                        'type'    => 'checkbox',
                        'label'   => __('Show navigation bullets?', 'coderehab-widgets-bundle'),
                        'default' => true,
                    ),

                    // Filter behaviour
                    'show_filter'     => array(
                        'type'          => 'checkbox',
                        'label'         => __('Add a background filter?', 'coderehab-widgets-bundle'),
                        'default'       => false,
                        'state_emitter' => array(
                            'callback' => 'conditional',
                            'args'     => array(
                                'use_filter_color[show]: val',
                                'use_filter_color[hide]: ! val',
                            ),
                        ),
                    ),

                    'filterSettings'  => array(
                        'type'          => 'section',
                        'label'         => __('Filter settings', 'coderehab-widgets-bundle'),
                        'hide'          => false,
                        'state_handler' => array(
                            'use_filter_color[show]' => array('show'),
                            'use_filter_color[hide]' => array('hide'),
                        ),

                        'fields'        => array(
                            'filter_color'   => array(
                                'type'          => 'color',
                                'label'         => __('Filter color', 'coderehab-widgets-bundle'),
                                'default'       => '#000',
                                'state_handler' => array(
                                    'use_filter_color[show]' => array('show'),
                                    'use_filter_color[hide]' => array('hide'),
                                ),
                            ),

                            'filter_opacity' => array(
                                'type'          => 'slider',
                                'label'         => __('Filter opacity (%)', 'coderehab-widgets-bundle'),
                                'default'       => 60,
                                'min'           => 0,
                                'max'           => 100,
                                'integer'       => true,
                                'state_handler' => array(
                                    'use_filter_color[show]' => array('show'),
                                    'use_filter_color[hide]' => array('hide'),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );
    }

    public function register_scripts_and_styles()
    {
        $this->register_frontend_styles(
            array(
                array(
                    'flickity',
                    get_template_directory_uri() . '/pagebuilder/widgets/custom/slider/assets/css/flickity.min.css',
                    array(),
                ),
            )
        );

        $this->register_frontend_scripts(
            array(
                array(
                    'flickity',
                    get_template_directory_uri() . '/pagebuilder/widgets/custom/slider/assets/js/flickity.min.js',
                    array('jquery'),
                ),
            )
        );
    }
}

siteorigin_widget_register('coderehab-slider', __FILE__, 'Coderehab_Slider');
