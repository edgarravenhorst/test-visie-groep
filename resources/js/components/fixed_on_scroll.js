export default class FixedOnScroll {
  constructor(selector, offset = 0) {
    this.offset = offset;
    this.$el = $(selector);
    if (!this.$el.length) return;

    this.menuTopOffset = this.$el.offset().top;
    $(window).scroll(e => this.onScrollChange(e));
  }

  onScrollChange(e) {
    if ($(window).scrollTop() > this.menuTopOffset + this.offset) {
      if (!this.$el.hasClass("fixed")) {
        this.$el.addClass("fixed");
      }
    } else {
      this.$el.removeClass("fixed");
    }
  }
}
