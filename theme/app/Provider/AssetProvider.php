<?php namespace Theme\Provider;

class AssetProvider
{
    public static function boot()
    {
        $provider = new AssetProvider();
        $provider->initialize();
        return $provider;
    }

    public function initialize()
    {
        $this->theme_dir         = get_template_directory_uri();
        $this->styles_dir  = $this->theme_dir."/assets/css";
        $this->scripts_dir = $this->theme_dir."/assets/js";

        add_action('wp_enqueue_scripts', [$this, "registerScripts"]);
    }

    public function registerScripts()
    {
        $this->registerScript("js-core-manifest", $this->scripts_dir."/manifest.js");
        $this->registerScript("js-core-vendor", $this->scripts_dir."/vendor.js");
        $this->registerScript("js-core-site", $this->scripts_dir."/site.js");

        $this->registerStyle("css-wp-default", $this->theme_dir."/style.css");
        $this->registerStyle("css-core-site", $this->styles_dir."/site.css");
    }

    public function registerStyle($name, $file, $deps=[], $enqueue=true, $media="all")
    {
        $version = date("ymd-Gis", $file_path);

        if ($enqueue) {
            wp_enqueue_style($name, $file, $deps, $version, $media);
        } else {
            wp_register_style($name, $file, $deps, $version, $media);
        }
    }

    public function registerScript($name, $file, $deps=[], $footer=true, $enqueue=true)
    {
        $version = date("ymd-Gis", $file_path);

        if ($enqueue) {
            wp_enqueue_script($name, $file, $deps, $version, $footer);
        } else {
            wp_register_script($name, $file, $deps, $version, $footer);
        }
    }
}
