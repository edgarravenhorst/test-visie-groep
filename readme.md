# Code.Rehab Core Theme

This is the code.rehab default Wordpress theme development boilerplate.

### Todo:

- Create a build script that creates a package without symlinks. (IF REQUIRED)
- Refactor some old code
- Extend route functionality
- Test intensively

### Requirements:

- wp-cli: https://wp-cli.org/
- yarn: https://yarnpkg.com/lang/en/
- composer: https://getcomposer.org/

### How to use:

First start by fetching the required packages including the latest Wordpress files:

    yarn

Once the packages have been installed it's mendatory to create a .env file.

    cp .env.example .env;

Initiate the setup command and wait for some magic to happen

    yarn setup;

For development run to watch files an recompile on change

    yarn watch;

### Demo content:

For a demo site import the backup that is provided. You can import this file by installing the All In One WP Migration plugin and run the import

### Now go create awesome stuff!!
