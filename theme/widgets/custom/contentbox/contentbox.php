<?php

/*
Widget Name: Contentbox
Description: A fullscreen block with big text.
Author: Code.Rehab
Author URI: http://code.rehab
 */

class Dation_Contentbox extends SiteOrigin_Widget
{

    public function __construct()
    {
        parent::__construct(
            'coderehab-contentbox',
            __('Contentbox', 'coderehab-widgets-bundle'),
            array(
                'description' => __('A fullscreen block with big text.', 'dation-widgets-bundle'),
            ),
            array(),
            false,
            plugin_dir_path(__FILE__)
        );
    }

    public function get_template_variables($instance, $args)
    {
        $background_image = wp_get_attachment_image_src($instance["background-image"], 'xxl')[0];

        $style = 'style="';
        $style = $instance["background-color"] ? $style . "background-color: " . $instance["background-color"] . ";" : $style;
        $style = $background_image ? $style . "background-image: url('" . $background_image . "');" : $style;
        $style = $instance["text-color"] ? $style . "color: " . $instance["text-color"] . ";" : $style;
        if ($instance['border']) {
            //$style = $instance["border-color"] ? $style . "color: " . $instance["text-color"] . ";" : $style;

            $style = $style . "border-style: solid;";
            $style = $instance["border-width"] ? $style . "border-width: " . $instance["border-width"] . ";" : $style;
            $style = $instance["border-color"] ? $style . "border-color: " . $instance["border-color"] . ";" : $style;
        }
        $style .= '"';

        return array(
            "content"         => $instance["content"],
            "style"           => $style,
            "button"          => $instance["button"],
            "shadow"          => $instance["shadow"],
            "rounded_corners" => $instance["rounded-corners"],
            "button_text"     => $instance["button-text"],
            "button_style"    => $instance["button-style"],
            "link"            => $instance["link"],
            "height"          => $instance["height"],
        );
    }

    public function get_widget_form()
    {
        return array(
            'content'          => array(
                'type'  => 'tinymce',
                'label' => __('Content', 'dation-widgets-bundle'),
            ),
            'height'           => array(
                'type'  => 'measurement',
                'label' => __('Height', 'dation-widgets-bundle'),
            ),
            'rounded-corners'  => array(
                'type'  => 'checkbox',
                'label' => __('Rounded Corners', 'dation-widgets-bundle'),
            ),
            'shadow'           => array(
                'type'  => 'checkbox',
                'label' => __('Shadow', 'dation-widgets-bundle'),
            ),
            'border'           => array(
                'type'  => 'checkbox',
                'label' => __('Border', 'dation-widgets-bundle'),
            ),
            'border-color'     => array(
                'type'  => 'color',
                'label' => __('Border color', 'dation-widgets-bundle'),
            ),
            'border-width'     => array(
                'type'  => 'measurement',
                'label' => __('Border width', 'dation-widgets-bundle'),
            ),
            'background-color' => array(
                'type'  => 'color',
                'label' => __('Background color', 'dation-widgets-bundle'),
            ),
            'text-color'       => array(
                'type'  => 'color',
                'label' => __('Text color', 'dation-widgets-bundle'),
            ),
            'background-image' => array(
                'type'     => 'media',
                'label'    => __('Background image', 'dation-widgets-bundle'),
                'choose'   => __('Select image', 'dation-widgets-bundle'),
                'update'   => __('Update image', 'dation-widgets-bundle'),
                'library'  => 'image',
                'fallback' => true,
            ),
            'button'           => array(
                'type'  => 'checkbox',
                'label' => __('Show Button', 'dation-widgets-bundle'),
            ),
            'button-text'      => array(
                'type'  => 'text',
                'label' => __('Button Text', 'dation-widgets-bundle'),
            ),
            'button-style'     => array(
                'type'    => 'select',
                'label'   => __('Button style', 'dation-widgets-bundle'),
                'options' => [
                    'btn-primary'               => "Dation Purple",
                    'btn-primary btn-outline'   => "Dation Purple, outline",
                    'btn-secondary'             => "Dation Red",
                    'btn-secondary btn-outline' => "Dation Red, outline",
                    'btn-white'                 => "White",
                    'btn-white btn-outline'     => "White, outline",
                ],
            ),
            'link'             => array(
                'type'  => 'link',
                'label' => __('Button link', 'dation-widgets-bundle'),
            ),
        );
    }

    public function get_template_name($instance)
    {
        return 'default';
    }

    public function get_style_name($instance)
    {
        return '';
    }
}
