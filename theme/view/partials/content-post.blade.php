<article @php post_class() @endphp>
  <header>
    <h2 class="entry-title">{{ get_the_title() }}</h2>
    {{-- @include('partials/entry-meta') --}}
  </header>
  <div class="entry">
    @php the_content() @endphp
  </div>
</article>
