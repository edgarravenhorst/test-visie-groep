<?php

/*
Widget Name: Core Unique Selling Points
Description: Summary off unique selling points
Author: Code.Rehab
Author URI: http://code.rehab
 */

class Coderehab_UniqueSellingPoints extends SiteOrigin_Widget
{
    public function __construct()
    {
        parent::__construct(
            'coderehab-unique-selling-points',
            __('Core Unique Selling Points', 'coderehab-widgets-bundle'),
            array(
                'description' => __('A fullscreen block with big text.', 'coderehab-widgets-bundle'),
                'help'        => __('https://code.rehab'),
            ),
            array(),
            false,
            plugin_dir_path(__FILE__)
        );
    }

    public function initialize()
    {
        add_image_size('banner-image', 1920, 1080, array('center', 'center')); // maybe better to select one from all registered sizes
        add_filter('siteorigin_widgets_template_html_coderehab-unique-selling-points', [$this, "render_blade_template"], 10, 3);

        global $blade;
        $blade->view()->addLocation(__DIR__ . "/view");

        // $this->register_scripts_and_styles();
    }

    public function get_less_variables($instance)
    {
        return [];
    }

    public function render_blade_template($template_html, $instance, $widget)
    {
        global $blade;
        return $blade->view()->make(
            "widget.core-unique-selling-points",
            [
                "content"          => $instance["content"],
                "selling_points" => $instance["selling_points"],
            ]
        );
    }

    public function get_widget_form()
    {
        return [
            // Content
            'content'          => array(
                'type'  => 'tinymce',
                'label' => __('Description', 'dation-widgets-bundle'),
            ),

            
            'selling_points' => array(
                'type'       => 'repeater',
                'label'      => __('Unique Selling points.', 'coderehab-widgets-bundle'),
                'item_name'  => __('Selling point', 'coderehab-widgets-bundle'),
                'item_label' => array(
                    'selector'     => "[id*='title']",
                    'update_event' => 'change',
                    'value_method' => 'val',
                ),

 
                'fields'     => array(
                    // Title field
                    'title' => array(
                        'type'  => 'text',
                        'label' => __('Slide title', 'coderehab-widgets-bundle'),
                    ),

                    // Content
                    'content'          => array(
                        'type'  => 'tinymce',
                        'label' => __('Description', 'dation-widgets-bundle'),
                    ),
                ),
            ),

        ];
    }
}

siteorigin_widget_register('coderehab-unique-selling-points', __FILE__, 'Coderehab_UniqueSellingPoints');
