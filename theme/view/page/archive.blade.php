@extends('layout.default')

@section("page-content")

  @if(!have_posts())
    <div class="alert alert-warning">
      <h4 class="mb-4"><?php echo __('Sorry, er zijn geen resultaten gevonden.', 'wpcore2019'); ?></h4>
    </div>
    {!! get_search_form(false) !!}
  @endif

  @while(have_posts()) @php the_post() @endphp
      @include('partials.content-loop')
      {{-- @include('partials.content-'.get_post_type()) --}}
  @endwhile

  {!! get_the_posts_navigation() !!}
@endsection
