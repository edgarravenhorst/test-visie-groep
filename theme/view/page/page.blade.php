@extends('layout.default')

@section("page-content")

  @while(have_posts())
    @php(the_post())
    @include('partials.content-page')
  @endwhile

@endsection
