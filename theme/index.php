<?php
use Theme\Router;

$router = new Router();

$router->when(is_front_page() && is_home(), "PostController@frontpage"); // Dynamic homepage
$router->when(is_front_page(), "PostController@frontpage"); // Static homepage
$router->when(is_home(), "PostController@archive"); // Blog page

$router->when(is_500(), "PostController@error500"); // 500 page
$router->when(is_404(), "PostController@error404"); // 404 page

$router->when(is_singular('product'), "PostController@single");
$router->when(is_singular('post'), "PostController@single");

$router->when(is_search(), "PostController@search");
$router->when(is_category(), "PostController@single");
$router->when(is_tag(), "PostController@single");
$router->when(is_archive(), "PostController@single");
$router->when(is_single(), "PostController@single");
$router->when(is_page(), "PostController@page");
