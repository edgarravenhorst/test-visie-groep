<section class="widget widget-unique-selling-points" >
  <section class="inner">
    
  <article>
    <header>
      <div class="content">
        @if(!empty($content))
          {!! $content !!}
        @endif
      </div>
      <div class="buttons">
        <a href="/over-ons" class="btn btn-lg btn-secondary full-width">Over ons</a>
        <a href="/shop" class="btn btn-lg btn-primary full-width">Bekijk onze lakens</a>
      </div>
    </header>

    <ul class="selling-points">
    @foreach($selling_points as $usp)
      @if(!empty($usp['title']) || !empty($usp['content']))
        <li class="usp">
          <h5 class="title">{{$usp['title']}}</h5>
          {!! $usp['content'] !!}
        </li>
      @endif
    @endforeach
    </ul>
</article>

  </section>
</section>